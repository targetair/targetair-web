/* eslint-disable no-unused-vars */
import axios from "axios";
axios.defaults.withCredentials = true;
axios.defaults.timeout = 180000;

// let host = process.env.BASE_URL || "";

let object = {};
object.catch = e => {
  let exceptionPath = ["/dev"];
  let isExceptionPath = exceptionPath.some(path => {
    return path == window.location.pathname;
  });
  if (isExceptionPath) return Promise.reject("서버에 문제가 있습니다.");
  if (e.response && e.response.status == 403) {
    return Promise.reject("로그인이 필요합니다.");
  } else if (e.response && e.response.status == 500) {
    return Promise.reject("서버에 문제가 있습니다.");
  } else {
    return Promise.reject("기타 오류가 있습니다.");
  }
};
object.store = null;
// object.setExpiredAt = expiredAt => {
//   object.store.state.expiredAt = expiredAt;
// };

object.request = function(method, url, params = {}, retry = 0) {
  let options = {
    method: method,
    // url: host + url
    url: url
  };

  if (method === "post" || method === "put") {
    options.data = params;
  } else {
    options.params = params;
  }

  return axios(options)
    .then(res => {
      //object.setExpiredAt(res.headers["expired-at"]);
      return Promise.resolve(res);
    })
    .catch(e => {
      return object.catch(e);
    });
};

object.requestMultipart = function(method, url, params = {}, retry = 0) {
  let options = {
    method: method,
    // url: host + url
    url: url,
    headers: {
      "Content-Type": "multipart/form-data",
      timeout: 60 * 1000 // 1min
    }
  };

  if (method === "post" || method === "put") {
    options.data = params;
  } else {
    options.params = params;
  }

  return axios(options)
    .then(res => {
      console.log("res", res);
      // object.setExpiredAt(res.headers["expired-at"]);
      return Promise.resolve(res);
    })
    .catch(e => {
      console.log("err", e);
      return object.catch(e);
    });
};

object.requestDownload = function(method, url, params = {}, retry = 0) {
  let options = {
    method: method,
    // url: host + url
    url: url,
    responseType: "blob"
  };

  if (method === "post" || method === "put") {
    options.data = params;
  } else {
    options.params = params;
  }

  return axios(options)
    .then(res => {
      // object.setExpiredAt(res.headers["expired-at"]);
      return Promise.resolve(res);
    })
    .catch(e => {
      return object.catch(e);
    });
};

// OCR Request
object.login = params => {
  return object.request("post", "/api/ocr/login", params);
};
object.getOcrRequestList = params => {
  return object.request("get", "/api/ocr/getOcrRequestList", params);
};
object.getOcrRequestStatistics = () => {
  return object.request("get", "/api/ocr/getOcrRequestStatistics");
};
object.getOcrRequestListAdmin = params => {
  return object.request("get", "/api/ocr/getOcrRequestListAdmin", params);
};
object.getOcrRequestStatisticsAdmin = () => {
  return object.request("get", "/api/ocr/getOcrRequestStatisticsAdmin");
};
object.getOcrRequestDetail = params => {
  return object.request("get", "/api/ocr/getOcrRequestDetail", params);
};
object.getOcrRequestResult = params => {
  return object.request("get", "/api/ocr/getOcrRequestResult", params);
};
object.createOcrRequest = params => {
  return object.requestMultipart("post", "/api/ocr/createOcrRequest", params);
};
object.updateOcrRequestStatus = params => {
  return object.request("post", "/api/ocr/updateOcrRequestStatus", params);
};
object.updateOcrRequestResult = params => {
  return object.request("post", "/api/ocr/updateOcrRequestResult", params);
};

// OCR TemplateProposal
object.getTmplProposalList = params => {
  return object.request("get", "/api/ocr/getTmplProposalList", params);
};
object.getTmplProposalListAdmin = params => {
  return object.request("get", "/api/ocr/getTmplProposalListAdmin", params);
};
object.createTemplateProposal = params => {
  // eslint-disable-next-line prettier/prettier
  return object.requestMultipart("post", "/api/ocr/createTemplateProposal", params);
};
object.updateTemplateProposal = params => {
  // eslint-disable-next-line prettier/prettier
  return object.requestMultipart("post", "/api/ocr/updateTemplateProposal", params);
};
object.updateTemplateProposalStatus = params => {
  // eslint-disable-next-line prettier/prettier
  return object.request("post", "/api/ocr/updateTemplateProposalStatus", params);
};
object.getTmplProposalDetail = params => {
  // eslint-disable-next-line prettier/prettier
  return object.requestMultipart("get", "/api/ocr/getTmplProposalDetail", params);
};

// OCR Template
object.getOcrTemplateList = params => {
  return object.request("get", "/api/ocr/getOcrTemplateList");
};
object.getOcrTemplateAvailableList = params => {
  return object.request("get", "/api/ocr/getOcrTemplateAvailableList");
};
object.getOcrTemplateDetail = params => {
  return object.request("get", "/api/ocr/getOcrTemplateDetail", params);
};
object.createOcrTemplate = params => {
  // eslint-disable-next-line prettier/prettier
  return object.requestMultipart("post", "/api/ocr/createOcrTemplate", params);
};
object.updateOcrTemplate = params => {
  // eslint-disable-next-line prettier/prettier
  return object.requestMultipart("post", "/api/ocr/updateOcrTemplate", params);
};
object.updateOcrTemplateStatus = params => {
  // eslint-disable-next-line prettier/prettier
  return object.request("post", "/api/ocr/updateOcrTemplateStatus", params);
};

export default object;
