import $ from './jquery-2.1.4.min.js'

//AJAX 전송하기
function AjaxTransfer(){
    this.isTransferring = false;
}

//POST 전송
AjaxTransfer.prototype.post = function(p){
    var t = this;
    var param = {
		  url  : null
		, type : 'POST'            //기본은 POST
		, dataType: 'json'         //수신 데이터 타입, 기본은 JSON
		, sendJson: true           //서버로 JSON 전송 시
		, data : null
		, onSuccess : null
		, onError : null
		, onComplete : null
		, isMultiPartForm : false  //파일업로드 구분
		, contentType : false
		, async : true            //비동기 처리 (기본은 비동기)
	};

    param = $.extend(param, p);

	if(!param.async && t.isTransferring){
		return;
	}

	delete param.async;

	t.isTransferring = true;

	if(param.sendJson && !param.isMultiPartForm){
		param.contentType = 'application/json; charset=utf-8';
		param.data = JSON.stringify(param.data);
	}

	if(param.isMultiPartForm){
		param.processData = false;
		param.contentType = false;

		var formData = new FormData();
		for (var key in param.data) {
	        if (!param.data.hasOwnProperty(key))
	            continue;
	        var value = param.data[key];
	        if(value instanceof Array){
	        	for(var i=0; i<value.length; i++){
	        		if(value[i] === undefined){
	        			continue;
	        		}
	        		formData.append(key + '[]', value[i]);
	        	}
	        	if(value.length == 0){
	        		formData.append(key + '[]', value);
	        	}
	        }else{
	        	if(value === undefined){
        			continue;
        		}
	        	formData.append(key, value);
	        }
	    }

        let xhRequest = new XMLHttpRequest;
        let multiPartPreSettings = {
              progressQueue          : []
            , progressControlTimeout : null
            , prevProgress           : 0
            , isProgressError        : false
        }
        let progressHandler =function(){
            if(multiPartPreSettings.isProgressError){
                multiPartPreSettings = {
                      progressQueue          : []
                    , progressControlTimeout : null
                    , prevProgress           : 0
                    , isProgressError        : false
                }
                return;
            }
            if(multiPartPreSettings.progressQueue.length > 0){
                let cp = multiPartPreSettings.progressQueue[0];
                multiPartPreSettings.progressQueue.remove(0);

                let stNum = cp.s;
                let intv = setInterval(function(){
                    param.onProgress(stNum);
                    if(stNum >= cp.e){
                        clearInterval(intv);
                    }
                    stNum++;
                }, 1);

                if(cp.e == 100){
                    multiPartPreSettings = {
                          progressQueue          : []
                        , progressControlTimeout : null
                        , prevProgress           : 0
                        , isProgressError        : false
                    }
                }else{
                    if(multiPartPreSettings.progressQueue.length > 0){
                        setTimeout(progressHandler, 1);
                    }else{
                        setTimeout(progressHandler, 100);
                    }
                }
            }else{
                setTimeout(progressHandler, 100);
            }
        };

        xhRequest.upload.onprogress = function(e) {

            if(param.onProgress !== undefined && param.onProgress != null){
                let percent = Math.floor(e.loaded / e.total * 100);
                multiPartPreSettings.progressQueue.push({s:multiPartPreSettings.prevProgress,e:percent});
                multiPartPreSettings.prevProgress = percent + 1;

                if(multiPartPreSettings.progressControlTimeout == null){
                    multiPartPreSettings.progressControlTimeout = setTimeout(progressHandler, 0);
                }
            }
        };
		xhRequest.addEventListener('load', function(){
			try{
				var data = JSON.parse(this.responseText);

                if(param.onSuccess != null && (param.onProgress === undefined || param.onProgress == null)){
                    param.onSuccess(data);
                }else if(param.onSuccess != null && param.onProgress !== undefined && param.onProgress != null){
                    let successAfterProgress = function(){
                        if(multiPartPreSettings.progressControlTimeout == null){
                            param.onSuccess(data);
                        }else{
                            setTimeout(successAfterProgress, 10);
                        }
                    };

                    setTimeout(successAfterProgress, 10);
                }

                t.isTransferring = false;
    			if(param.onComplete != null){
    				param.onComplete();
    			}
			}catch(e){
				if(param.onError != null){
                    param.onError(e);
				}
                multiPartPreSettings.isProgressError = true;
                t.isTransferring = false;
    			if(param.onComplete != null){
    				param.onComplete();
    			}
			}

		});
		xhRequest.addEventListener('error', function(e){
			if(param.onError != null){
                param.onError(e);
			}
            multiPartPreSettings.isProgressError = true;
            t.isTransferring = false;
		}, false);
		xhRequest.open("POST", param.url);
		xhRequest.send(formData);
	}else{
		param.success = function(data){
            if(param.onSuccess != null){
                param.onSuccess(data);
            }
		};
		param.error = function(data){
			if(param.onError != null){
				param.onError(e);
			}
		};
		param.complete = function(data){
			t.isTransferring = false;
			if(param.onComplete != null){
				param.onComplete();
			}
		};

		$.ajax(param);
	}
}

const Ajax = new AjaxTransfer();

export default Ajax;
