import $ from '../common/jquery-2.1.4.min.js'
import Ajax from '../common/util.js'

const mapping = {
      idCard                : 'id-card'
    , driversLicense        : 'drivers-license'
    , idCardReqDoc          : 'id-card-req-doc'
    , businessLicense       : 'business-license'
    , houseMembers          : 'house-members'
    , officialPersonReg     : 'official-person-reg'
    , familyCert            : 'family-cert'
    , militaryServiceNotice : 'military-service-notice'
    , stampCert             : 'stamp-cert'
    , uncategorized         : 'uncategorized'
};

function Category(){
    let t = this;

    t.docNum = 0;

    t.contents = {
        businessLicense         : []
      , familyCert              : []
      , driversLicense          : []
      , idCardReqDoc            : []
      , idCard                  : []
      , stampCert               : []
      , officialPersonReg       : []
      , houseMembers            : []
      , militaryServiceNotice   : []
      , uncategorized           : []
    };

}

Category.prototype.registEvents = function(){
    let t = this;

    Object.keys(mapping).forEach(key => {
        $('.item-wrap .item.' + mapping[key]).on('click', function(){
            if($(this).hasClass('selected')) return;

            $('.item-wrap .item').removeClass('selected');
            t.container.empty();
            $(this).addClass('selected');

            let array = t.contents[key];
            for(let i=0; i<array.length; i++){
                array[i].draw();
            }
        });
    });
}

Category.prototype.init = function(){
    let t = this;

    t.docNum = 0;

    t.contents = {
        businessLicense         : []
      , familyCert              : []
      , driversLicense          : []
      , idCardReqDoc            : []
      , idCard                  : []
      , stampCert               : []
      , officialPersonReg       : []
      , houseMembers            : []
      , militaryServiceNotice   : []
      , uncategorized           : []
    };

}

Category.prototype.add = function(doc){
    let t = this;

    t.contents[doc.type].push(doc);

    let cat = $('.' + mapping[doc.type] + ' .cat-num');
    let curNum = Number(cat.html());
    curNum++;

    cat.html(curNum);

    t.docNum++;

    if(t.docNum == 1){
        t.firstAddType = doc.type;
    }

    let progress = t.docNum / t.preDocNum;
    let gageWidth = AiIntro.fileDropper.find('.progress-rail').width() * progress;
    AiIntro.fileDropper.find('.progress-gage').animate({width:gageWidth}, 10);

    if(t.docNum == t.preDocNum){
        let keyArray = Object.keys(t.contents);
        for(let i=0; i<keyArray.length; i++){
            let key = keyArray[i];
            if(t.contents[key].length > 0){
                $('.item-wrap .item').removeClass('selected');
                t.container.empty();
                $('.item-wrap .item.' + mapping[key]).addClass('selected');

                let array = t.contents[key];
                for(let j=0; j<array.length; j++){
                    array[j].draw();
                    AiIntro.fileDropper.find('.v-messages__wrapper .progress-rail').remove();
                    AiIntro.fileDropper.find('.v-messages__message').removeClass('hidden');
                }

                break;
            }
        }

    }
}

Category.prototype.setContainer = function(container){
    let t = this;
    t.container = container;
}

Category.prototype.setPreDocNum = function(preDocNum){
    let t = this;
    t.preDocNum = preDocNum;
}

const category = new Category();

function Document(param){
    let t = this;
    t.param = param;
    let size = param.file.size;
    let fileReader = new FileReader();

    t.table = $('.v-data-table__wrapper tbody');

    try{
        fileReader.readAsDataURL(param.file);
        fileReader.onload = function(e){

            t.imgSrc = e.target.result;

            let img = new Image();

            img.onload = function(){
                let canvas = $('<canvas>');
                canvas[0].width = 150;
                canvas[0].height = 200;

                let width     = this.width;
                let height    = this.height;
                let newWidth  = canvas[0].width;
                let newHeight = canvas[0].height;

                let left = 0;
                let top  = 0;

                if(width > height){
                    newHeight = (height * newWidth) / width;
                    top = (canvas[0].height - newHeight) / 2;
                }else{
                    newWidth = (width * newHeight) / height;
                    left = (canvas[0].width - newWidth) / 2;
                }

                let context = canvas[0].getContext('2d');
                context.drawImage(
                    img, 0, 0, width, height, left, top, newWidth, newHeight
                );

                t.imageConetnts = canvas[0].toDataURL('image/png');

                //분석결과
                Ajax.post({
                      url : 'http://targetair.iptime.org:9500/sort'
                    , isMultiPartForm : true
                    , data  : {file : param.file}
                    , onSuccess : function(data){
                        let typeCode = Number(data.typeCode);
                        t.typeName = data.typeName;

                        switch(typeCode){
                            case 0 :
                                t.type = 'businessLicense'
                                category.add(t);
                                break;
                            case 1 :
                                t.type = 'familyCert'
                                category.add(t);
                                break;
                            case 2 :
                                t.type = 'driversLicense'
                                category.add(t);
                                break;
                            case 3 :
                                t.type = 'idCardReqDoc'
                                category.add(t);
                                break;
                            case 4 :
                                t.type = 'idCard'
                                category.add(t);
                                break;
                            case 5 :
                                t.type = 'stampCert'
                                category.add(t);
                                break;
                            case 6 :
                                t.type = 'officialPersonReg'
                                category.add(t);
                                break;
                            case 7 :
                                t.type = 'houseMembers'
                                category.add(t);
                                break;
                            case 8 :
                                t.type = 'militaryServiceNotice'
                                category.add(t);
                                break;
                            case 9 :
                                t.type = 'uncategorized'
                                category.add(t);
                                break;
                            case 10 :
                                t.type = 'uncategorized'
                                category.add(t);
                                break;
                            default :
                                t.type = 'uncategorized'
                                category.add(t);
                                break;
                        }
                    }
                    , onError : function(e){
                        AiIntro.fileDropper.find('.v-messages__wrapper .progress-rail').remove();
                        AiIntro.fileDropper.find('.v-messages__message').removeClass('hidden');
                        alert('문서 분석에 실패하였습니다.');
                    }
                });
            }

            img.src = t.imgSrc;
        }
    }catch(err){
        alert('파일 업로드 중 오류가 발생했습니다.')
    }
}

Document.prototype.draw = function(){
    let t = this;

    let doc = $('<div class="doc-wrapper"><div class="doc-item"></div></div>');
    t.param.container.append(doc);

    doc.find('.doc-item').css('background-image', 'url('+ t.imageConetnts +')');

    let img = new Image();

    img.onload = function(){
        let canvas = $('<canvas>');
        canvas[0].width = this.width;
        canvas[0].height = this.height;
        let context = canvas[0].getContext('2d');
        context.drawImage(
            img, 0, 0, this.width, this.height, 0, 0, this.width, this.height
        );
        t.originalImageContents = canvas[0].toDataURL('image/png');
    }
    img.src = t.imgSrc;

    if(t.type == 'businessLicense'){
        doc.find('.doc-item').addClass('type-biz');
        doc.find('.doc-item').append('<div class="ocr-gate">OCR <br> 체험하기</div>');
    }

    doc.find('.doc-item .doc-item, .doc-item .ocr-gate').on('click', function(){
        t.table.find('tr').remove();

        $('#analysis-table').append('<div class="an-ing" style="text-align:center;padding-top:100px;font-weight: bold;">분석중입니다.<div>');

        if($(this).hasClass('ocr-gate')){
            //분석결과
            Ajax.post({
                  url : 'http://targetair.iptime.org:9500/ocr_bf'
                , isMultiPartForm : true
                , data  : {file : t.param.file}
                , onSuccess : function(data){
                    t.analyzeData = data;
                    $('#analysis-table .an-ing').remove();
                    for(let i=0; i<data.length; i++){
                        let rowData = data[i];
                        t.table.append('<tr><td>'+rowData.no+'</td><td>'+rowData.category+'</td><td>'+rowData.contents+'</td></tr>')
                    }

                }
                , onError : function(e){
                    alert('OCR 분석에 실패하였습니다.');
                }
            });
        }
    });

    let magnify = $('<div class="magnify"></div>');
    doc.find('.doc-item').append(magnify);

    magnify.on('click', function(e){
        e.stopPropagation();
        t.table.find('tr').remove();
        if(t.analyzeData != null && t.analyzeData !== undefined){
            for(let i=0; i< t.analyzeData.length; i++){
                let rowData =  t.analyzeData[i];
                t.table.append('<tr><td>'+rowData.no+'</td><td>'+rowData.category+'</td><td>'+rowData.contents+'</td></tr>')
            }
            $('.image-pop-wrap .image-pop').addClass('both-layer');
            $('.image-pop-wrap .image-pop .ocr-half').html($('#analysis-table').clone());
        }else{
            $('.image-pop-wrap .image-pop').removeClass('both-layer');
            $('.image-pop-wrap .image-pop .ocr-half').empty();
        }
        $('.image-pop-wrap .image-pop .image-half').css('background-image', 'url('+t.originalImageContents+')');

        $('.image-pop-wrap').removeClass('hidden');
    });
}

const AiIntro = {
    init : function(){
        $('#ai-intro .id-card .cat-num')                .html(0);
        $('#ai-intro .drivers-license .cat-num')        .html(0);
        $('#ai-intro .id-card-req-doc .cat-num')        .html(0);
        $('#ai-intro .business-license .cat-num')       .html(0);
        $('#ai-intro .house-members .cat-num')          .html(0);
        $('#ai-intro .official-person-reg .cat-num')    .html(0);
        $('#ai-intro .family-cert .cat-num')            .html(0);
        $('#ai-intro .military-service-notice .cat-num').html(0);
        $('#ai-intro .stamp-cert .cat-num')             .html(0);
        $('#ai-intro .uncategorized .cat-num')          .html(0);

        category.init();

        $('#ai-intro .cat-img-container .in1').empty();
        $('#ai-intro .item-wrap .item').removeClass('selected');

        $('.v-data-table__wrapper tbody tr').remove();
    },

    uploadFiles : function(fileProvider){
        let fileLength = fileProvider.files.length;
        if(fileLength < 1){
            return;
        }

        if(fileLength > 20){
            return;
        }

        category.setPreDocNum(fileLength);

        AiIntro.fileDropper.find('.v-messages__message').addClass('hidden');
        AiIntro.fileDropper.find('.v-messages__wrapper').append('<div class="progress-rail" style="height:14px;"><div class="progress-gage"></div></div>')

        for(let i=0; i<fileLength; i++){
            let file = fileProvider.files[i];

            if(file.type.indexOf('image') < 0){
                continue;
            }

            new Document({
                  file      : file
                , container : $('.cat-img-container .in1')
            });


        }

        //최종 마무리
        AiIntro.fileDropper.removeClass('uploading');

        let fileInput = $('#file-droppper input[type="file"]');
        if(/msie/i.test(navigator.userAgent.toLowerCase())){
            fileInput.replaceWith(fileInput.clone(true));
        }else{
            fileInput.val('');
        }
    },

    registEvents : function(){

        AiIntro.fileDropper = $('#ai-intro #file-droppper');

        $(document).on('dragover dragenter drop', function(e){
            e.stopPropagation();
            e.preventDefault();
        });

        let fileInput = $('#ai-intro #file-droppper input[type="file"]');

        AiIntro.fileDropper.on('dragover dragenter', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).addClass('drag-over');
        });

        AiIntro.fileDropper.find('.sign').on('dragleave', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).removeClass('drag-over');
        });

        $('#ai-intro #file-droppper .sign').on('drop', function(e){
            e.stopPropagation();
            e.preventDefault();
        });

        function setUploadInfo(fileProvider){
            let fileLength = fileProvider.files.length;
            if(fileLength == 1){
                let file = fileProvider.files[0];
                let contents = file.name;
                let size = file.size;
                if(size < 1024){
                    contents += ' ('+ size +' B)';
                }else{
                    contents += ' ('+ (size/1024).toFixed(1) +' kB)';
                }
                AiIntro.fileDropper.find('.v-file-input__text').html(contents);
            }else if(fileLength > 1 && fileLength <= 20){

                let totSize = 0;
                for(let i=0; i<fileLength; i++){
                    let file = fileProvider.files[i];
                    let size = file.size;
                    totSize = totSize + size;
                }

                let contents = fileLength + ' files';

                if(totSize < 1024){
                    contents += ' ('+ totSize +' B in total)';
                }else{
                    contents += ' ('+ (totSize/1024).toFixed(1) +' kB in total)';
                }

                AiIntro.fileDropper.find('.v-file-input__text').html(contents);
            }else{
                return;
            }

            AiIntro.fileDropper.find('v-input').addClass('v-input--is-label-active v-input--is-dirty');
            AiIntro.fileDropper.find('label').addClass('v-label--active');
        }

        AiIntro.fileDropper.on('drop', function(e){
            e.preventDefault();
            let t = $(this);
            t.removeClass('drag-over');
            let fileProvider = e.originalEvent.dataTransfer;
            let fileLength = fileProvider.files.length;

            if(fileLength <= 0){
                return;
            }else{
                setUploadInfo(fileProvider);
            }
            AiIntro.init();
            t.addClass('uploading');
            AiIntro.uploadFiles(fileProvider);
        });

        AiIntro.fileDropper.find('.sign').on('drop', function(e){
            e.preventDefault();
            let t = $(this);
            if(t.parent().hasClass('uploading')) return;

            t.parent().removeClass('drag-over');
            let fileProvider = e.originalEvent.dataTransfer;
            let fileLength = fileProvider.files.length;

            if(fileLength <= 0){
                return;
            }else{
                setUploadInfo(fileProvider);
            }
            AiIntro.init();
            t.parent().addClass('uploading');
            AiIntro.uploadFiles(fileProvider);
        });

        fileInput.on('change', function(){

            let fileProvider = this;
            let fileLength = fileProvider.files.length;

            if(fileLength <= 0){
                return;
            }else{
                setUploadInfo(fileProvider);
            }
            AiIntro.init();
            setUploadInfo(fileProvider);
            AiIntro.fileDropper.addClass('uploading');
            AiIntro.uploadFiles(fileProvider);
        });

        $('#ai-intro .image-pop-wrap .image-close').on('click', function(){
            $(this).parent().addClass('hidden');
        });

        AiIntro.fileDropper.find('label').html('');
        AiIntro.fileDropper.find('button.mdi.mdi-close').remove();

        category.registEvents();
        category.setContainer($('#ai-intro .cat-img-container .in1'));
    }

}



export default AiIntro;
