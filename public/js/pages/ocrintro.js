import $ from '../common/jquery-2.1.4.min.js'
import Ajax from '../common/util.js'

function Document(isLoad, param){
    let t = this;

    t.table = $('.v-data-table__wrapper tbody');

    let thumbContainer = $('.thumb-container .thumb-guid');
    let thumbWrapper = $('<div class="thumb-wrapper"></div>');
    let thumbItem = $('<div class="thumb-item"></div>');
    thumbWrapper.append(thumbItem);

    if(isLoad){
        $('.doc-viewer .v-image__image').css('background-image', 'url('+ param.image +')');
        thumbItem.addClass('selected');
        t.table.find('tr').remove();
        for(let i=0; i<param.analyzeData.length; i++){
            let rowData = param.analyzeData[i];
            t.table.append('<tr><td>'+rowData.no+'</td><td>'+rowData.category+'</td><td>'+rowData.contents+'</td></tr>')
        }
    }

    thumbItem.css('background-image', 'url('+ param.image +')');

    thumbContainer.append(thumbWrapper);

    thumbItem.on('click', function(){
        if($(this).hasClass('selected')) return;
        $('.thumb-item').removeClass('selected');
        $(this).addClass('selected');

        $('.doc-viewer .v-image__image').css('background-image', 'url('+ param.image +')');
        $('#doc-category-result font').html(param.typeName);

        t.table.find('tr').remove();
        for(let i=0; i<param.analyzeData.length; i++){
            let rowData = param.analyzeData[i];
            t.table.append('<tr><td>'+rowData.no+'</td><td>'+rowData.category+'</td><td>'+rowData.contents+'</td></tr>')
        }
    });
}

const OcrIntro = {
    init : function(){
        OcrIntro.fileDropper = $('#ocr-intro #file-droppper');
        new Document(
              true
            , {   image : '/img/img_1604048131605375.253a5d83.png'
                , typeName : '사업자등록증'
                , analyzeData : [
                      {no : '1', category : '사업자유형', contents : '법인사업자'}
                    , {no : '2', category : '등록번호', contents : '117-81-65799'}
                    , {no : '3', category : '법인명(단체명)', contents : '주식회사 케이티디에스'}
                    , {no : '4', category : '대표자', contents : '우정민'}
                ]
        });
    },

    showImagePopup : function(){
        $('.image-pop-wrap .image-pop').css('background-image', $('.doc-viewer .v-image__image').css('background-image'));

        $('.image-pop-wrap').removeClass('hidden');
    },

    uploadFiles : function(fileProvider){
        let fileLength = fileProvider.files.length;
        if(fileLength != 1){
            return;
        }

        for(let i=0; i<fileLength; i++){
            let file = fileProvider.files[i];

            if(file.type.indexOf('image') < 0){
                continue;
            }

            let fileReader = new FileReader();

            try{
                fileReader.readAsDataURL(file);

                fileReader.onload = function(e){

                    let imgSrc = e.target.result;

                    let img = new Image();

                    img.onload = function(){
                        let canvas = $('<canvas>');
                        canvas[0].width = this.width;
                        canvas[0].height = this.height;


                        let context = canvas[0].getContext('2d');
                        context.drawImage(
                            img, 0, 0, this.width, this.height, 0, 0, this.width, this.height
                        );

                        let ocrImageConetnts = canvas[0].toDataURL('image/png');

                        //분석결과
                        Ajax.post({
                              url : 'http://targetair.iptime.org:9500/sort'
                            , isMultiPartForm : true
                            , data  : {file : file}
                            , onSuccess : function(data){
                                let typeName = data.typeName;

                                Ajax.post({
                                      url : 'http://targetair.iptime.org:9500/ocr_bf'
                                    , isMultiPartForm : true
                                    , data  : {file : file}
                                    , onSuccess : function(data){
                                        new Document(
                                              false
                                            , {
                                                  image       : ocrImageConetnts
                                                , typeName    : typeName
                                                , analyzeData : data
                                            }
                                        );
                                    }
                                    , onError : function(e){
                                        alert('OCR 분석에 실패하였습니다.');
                                    }
                                });
                            }
                            , onError : function(e){
                                alert('문서 분석에 실패하였습니다.');
                            }
                        });

                    }
                    img.src = imgSrc;
                }
            }catch(err){
                alert('파일 업로드 중 오류가 발생했습니다.')
            }
        }

        //최종 마무리
        OcrIntro.fileDropper.removeClass('uploading');

        let fileInput = $('#file-droppper input[type="file"]');
        if(/msie/i.test(navigator.userAgent.toLowerCase())){
            fileInput.replaceWith(fileInput.clone(true));
        }else{
            fileInput.val('');
        }


    },

    registEvents : function(){
        $(document).on('dragover dragenter drop', function(e){
            e.stopPropagation();
            e.preventDefault();
        });

        let fileInput = $('#ocr-intro #file-droppper input[type="file"]');

        OcrIntro.fileDropper.on('dragover dragenter', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).addClass('drag-over');
        });

        OcrIntro.fileDropper.find('.sign').on('dragleave', function(e){
            e.stopPropagation();
            e.preventDefault();
            $(this).removeClass('drag-over');
        });

        $('#ocr-intro #file-droppper .sign').on('drop', function(e){
            e.stopPropagation();
            e.preventDefault();
        });

        $('.to-aiintro').on('click', function(){
            location.href = '/aiintro';
        });

        function setUploadInfo(fileProvider){
            let fileLength = fileProvider.files.length;
            if(fileLength == 1){
                let file = fileProvider.files[0];
                let contents = file.name;
                let size = file.size;
                if(size < 1024){
                    contents += ' ('+ size +' B)';
                }else{
                    contents += ' ('+ (size/1024).toFixed(1) +' kB)';
                }
                OcrIntro.fileDropper.find('.v-file-input__text').html(contents);
            }else if(fileLength > 1 && fileLength <= 20){

                let totSize = 0;
                for(let i=0; i<fileLength; i++){
                    let file = fileProvider.files[i];
                    let size = file.size;
                    totSize = totSize + size;
                }

                let contents = fileLength + ' files';

                if(totSize < 1024){
                    contents += ' ('+ totSize +' B in total)';
                }else{
                    contents += ' ('+ (totSize/1024).toFixed(1) +' kB in total)';
                }

                OcrIntro.fileDropper.find('.v-file-input__text').html(contents);
            }else{
                return;
            }

            OcrIntro.fileDropper.find('v-input').addClass('v-input--is-label-active v-input--is-dirty');
            OcrIntro.fileDropper.find('label').addClass('v-label--active');
        }

        OcrIntro.fileDropper.on('drop', function(e){
            e.preventDefault();
            let t = $(this);
            t.removeClass('drag-over');
            let fileProvider = e.originalEvent.dataTransfer;
            let fileLength = fileProvider.files.length;

            if(fileLength != 1){
                return;
            }else{
                setUploadInfo(fileProvider);
            }
            t.addClass('uploading');
            OcrIntro.uploadFiles(fileProvider);
        });

        OcrIntro.fileDropper.find('.sign').on('drop', function(e){
            e.preventDefault();
            let t = $(this);
            if(t.parent().hasClass('uploading')) return;

            t.parent().removeClass('drag-over');
            let fileProvider = e.originalEvent.dataTransfer;
            let fileLength = fileProvider.files.length;

            if(fileLength != 1){
                return;
            }else{
                setUploadInfo(fileProvider);
            }
            t.parent().addClass('uploading');
            OcrIntro.uploadFiles(fileProvider);
        });

        fileInput.on('change', function(){

            let fileProvider = this;
            let fileLength = fileProvider.files.length;

            if(fileLength <= 0){
                return;
            }else{
                setUploadInfo(fileProvider);
            }
            setUploadInfo(fileProvider);
            OcrIntro.fileDropper.addClass('uploading');
            OcrIntro.uploadFiles(fileProvider);
        });

        $('#ocr-intro .image-pop-wrap .image-close').on('click', function(){
            $(this).parent().addClass('hidden');
        });

        OcrIntro.fileDropper.find('label').html('');
        OcrIntro.fileDropper.find('button.mdi.mdi-close').remove();


    }
}

export default OcrIntro;
