import "babel-polyfill";
import "es6-promise/auto";
//import "element-closest-polyfill";

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
//import dayjs from "dayjs";
import moment from "moment";
import VueCookies from "vue-cookies"; // nh
import http from "../plugins/http"; // nh
import numeral from "numeral";
import "@/plugins/axios";
import VueSweetalert2 from "vue-sweetalert2";
// shared component
import PageDescriber from "@/components/shared/PageDescriber.vue";

Vue.config.productionTip = false;

Vue.use(VueSweetalert2);
Vue.use(VueCookies); // nh
Vue.prototype.$http = http; // nh

Vue.component(PageDescriber.name, PageDescriber);

Vue.filter("formatDate", function(value, format) {
  if (value) {
    return moment(value).format(format);
  }
});

Vue.filter("formatNumber", targetNum => {
  if (targetNum) return numeral(targetNum).format("0,0");
});

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
