/* eslint-disable no-unused-vars */
import * as types from "./mutation-types";
import Vue from "vue";
import http from "../..//plugins/http"; // nh
// 각 업무에 맞춰 api 서버를 분리한다.
import { Auth, List, Task } from "../api";

export default {
  login: ({ commit }, authInfo) => {
    return new Promise((resolve, reject) => {
      console.log("authInfo", authInfo);
      // FIXME: for test
      if (!authInfo) {
        reject("authInfo 객체는 필수입니다.");
      } else {
        if (authInfo.password != "1111") {
          reject("Invlid account or passwaord.");
        }

        let authorizedUserInfo = {};

        switch (authInfo.userId) {
          case "developer1":
            break;
          case "developer2":
            break;
          default:
            reject("Invlid account or passwaord.");
        }

        var requestParam = {
          userId: authInfo.userId
        };

        let authInfoResult = {};

        return http
          .login(requestParam)
          .then(res => {
            console.log("login 완료");
            console.log("ress", res);
            authInfoResult = res.data.data;
            commit(types.AUTH_LOGIN, authInfoResult);
            // Vue.cookies.set("account", authInfoResult);
            // Vue.session.set("user", authInfoResult);

            resolve(authInfoResult);
          })
          .catch(err => {
            reject(err);
          });
      }
    });

    // TODO: Auth API 에 구현한 비즈니스 로직을 호출한다.
    // eslint-disable-next-line no-unreachable
    // return Auth.login(authInfo)
    //   .then(({ token, userId, email, deptName }) => {
    //     commit(types.AUTH_LOGIN, { token, userId, email, deptName });
    //   })
    //   .catch(err => {
    //     throw err;
    //   });
    // throw new Error("login action 은 반드시 구현되어야 합니다.");
  },

  logout: ({ commit }) => {
    // TODO:
    return commit(types.AUTH_LOGOUT);
    // throw new Error("logout action 은 반드시 구현되어야 합니다.");
  },

  fetchLists: ({ commit }) => {
    // TODO:
    throw new Error("fetchLists action 은 반드시 구현되어야 합니다.");
  },

  addTask: ({ commit }) => {
    // TODO:
    throw new Error("addTask action 은 반드시 구현되어야 합니다.");
  },

  updateTask: ({ commit }) => {
    // TODO:
    throw new Error("updateTask action 은 반드시 구현되어야 합니다.");
  },

  removeTask: ({ commit }) => {
    // TODO:
    throw new Error("removeTask action 은 반드시 구현되어야 합니다.");
  }
};
