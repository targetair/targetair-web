module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    disableHostCheck: true,
    proxy: {
      "/apis": {
        //target: "http://nh-rpa-ocr-web.ktdscoe.myds.me",
        target: "http://targetair.iptime.org:9081",
        changeOrigin: true
      },
      "/api": {
        //target: "http://nh-rpa-ocr-web.ktdscoe.myds.me   ",
        target: "http://targetair.iptime.org:9081",
        changeOrigin: true
      }
    }
  }
};
